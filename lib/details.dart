import 'dart:developer';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:myfirebase/_userDetails.dart';
import 'package:flutter/material.dart';

class Details extends StatefulWidget {
  const Details({Key? key}) : super(key: key);

  @override
  State<Details> createState() => _DetailsState();
}

class _DetailsState extends State<Details> {
  @override
  Widget build(BuildContext context) {
    TextEditingController nameController = TextEditingController();
    TextEditingController surnameController = TextEditingController();
    TextEditingController emailController = TextEditingController();

    Future Details() {
      final name = nameController.text;
      final surname = surnameController.text;
      final email = emailController.text;

      final ref = FirebaseFirestore.instance.collection("Detail").doc();

      return ref
          .set({
            "Name": name,
            "Surname": surname,
            "Email": email,
            "doc_id": ref.id
          })
          .then((value) => log("Collection successfull"))
          .catchError((onError) => log(onError));
    }

    return Column(
      children: [
        Column(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
              child: TextField(
                  controller: nameController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Name')),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
              child: TextField(
                  controller: surnameController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Surname')),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: 4, vertical: 6),
              child: TextField(
                  controller: emailController,
                  decoration: InputDecoration(
                      border: OutlineInputBorder(), hintText: 'Email')),
            ),
            ElevatedButton(
                onPressed: () {
                  Details();
                },
                child: const Text('Submit'))
          ],
        ),
      ],
    );
  }
}
