import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:myfirebase/Details.dart';

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
      options: const FirebaseOptions(
          apiKey: "AIzaSyC-tD0W-qch5PAD4QwZibDX0Au5PgfN8Wk",
          authDomain: "appacademy-dfc8a.firebaseapp.com",
          projectId: "appacademy-dfc8a",
          storageBucket: "appacademy-dfc8a.appspot.com",
          messagingSenderId: "1053733160839",
          appId: "1:1053733160839:web:7136a6cd5cb80b9e9d3522",
          measurementId: "G-YP0XJ9QHTD"));

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Register',
      theme: ThemeData(
        primarySwatch: Colors.green,
      ),
      home: const MyHomePage(title: 'Registration'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Registration'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: const <Widget>[Details()],
        ),
      ),
    );
  }
}
